const puppeteer = require('puppeteer');
const config = require('config');


function delay(time) {
    return new Promise(function(resolve) {
        setTimeout(resolve, time);
    });
 }

async function saveQuestion(questionId) {
    console.log("+ Collecting question " + questionId)

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    let isQuestionATable = false;

    // If questionId ends with a "t", the question is a table, and we'll later
    // change the viewport to show it completely
    if (questionId.slice(-1) == "t") {
        questionId = questionId.slice(0, -1);
        isQuestionATable = true;
    }

    let metabaseUrl = config.get('metabase.url');
    let metabaseExternalUrl = config.get('metabase.externalUrl');

    // Information about the question
    question = {};
    question.id = questionId;
    question.imagePath = `questions/question_${questionId}.png`;
    question.url = `${metabaseUrl}question/${questionId}`;
    question.externalUrl = `${metabaseExternalUrl}question/${questionId}`;

    // Entering the login page
    await page.goto(metabaseUrl);
    await page.waitFor('input[name=username]');

    // Logging in
    await page.type('input[name=username]', config.get('metabase.user'));
    await page.type('input[name=password]', config.get('metabase.password'));
    await page.keyboard.press('Enter');

    // Waiting for page to load
    await page.waitForFunction( () => document.querySelector("body").innerText.includes('NOSSOS DADOS'));

    // Acessing the question
    await page.goto(question.url, {waitUntil: 'networkidle2'});
    const questionTimeout = 60 * 4 * 1000;
    await page.waitForFunction( () => !document.querySelector(".Loading-message"), { timeout: questionTimeout });

    // Get question name
    const text = await page.$('.mr1.Subhead-cNCOMx.hiDbPY');
    question.name = await page.evaluate(el => el.textContent, text);

    // Change viewport if question is a table
    if (isQuestionATable) {
        /* Make the viewport as big as possible. This will be our maximum size
           for the table. */
        await page.setViewport({
            width: 3000,
            height: 600,
        });
        // Wait for the readaptation to complete
        await page.goto(question.url, {waitUntil: 'networkidle2'});

        // Get the size of the table
        const table = await page.$('div.ReactVirtualized__Grid:nth-child(2) > div:nth-child(1)');
        const bounding_box = await table.boundingBox();

        // Set viewport width as the same size as the width of the table
        await page.setViewport({
            width: bounding_box.width,
            height: 600,
        });
        await page.goto(question.url, {waitUntil: 'networkidle2'});
    }

    // Save image of question and close browser
    const graph = await page.$('.Visualization');
    await graph.screenshot({path: question.imagePath});
    await browser.close();

    console.log("- Question " + questionId + " collected.");
    return question;
};

module.exports = saveQuestion;
