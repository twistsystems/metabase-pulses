const puppeteer = require('puppeteer');
const config = require('config');


function delay(time) {
    return new Promise(function(resolve) {
        setTimeout(resolve, time);
    });
 };

async function saveDashboard(dashboardId) {
    console.log("+ Collecting dashboard " + dashboardId)

    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // Uncomment if you want the screenshot with a different
    /*await page.setViewport({
        width: 900,
        height: 600,
        deviceScaleFactor: 1,
    });*/

    let metabaseUrl = config.get('metabase.url');
    let metabaseExternalUrl = config.get('metabase.externalUrl');

    // Information about the dashboard
    dashboard = {};
    dashboard.id = dashboardId;
    dashboard.imagePath = `dashboards/dashboard_${dashboardId}.png`;
    dashboard.url = `${metabaseUrl}dashboard/${dashboardId}`;
    dashboard.externalUrl = `${metabaseExternalUrl}dashboard/${dashboardId}`;

    // Entering the login page
    await page.goto(metabaseUrl);
    await page.waitFor('input[name=username]');

    // Logging in
    await page.type('input[name=username]', config.get('metabase.user'));
    await page.type('input[name=password]', config.get('metabase.password'));
    await page.keyboard.press('Enter');

    // Waiting for page to load
    await page.waitForFunction( () => document.querySelector("body").innerText.includes('NOSSOS DADOS'));

    // Acessing the dashboard
    await page.goto(dashboard.url, { waitUntil: 'networkidle2' });
    const dashboardTimeout = 60 * 4 * 1000;
    await page.waitForFunction( () => !document.querySelector(".Loading-message"), { timeout: dashboardTimeout });

    // Get dashboard name
    const text = await page.$('.h2');
    dashboard.name = await page.evaluate(el => el.textContent, text);

    // Save image of dashboard and close browser
    const graph = await page.$('.DashboardGrid');
    await graph.screenshot({path: dashboard.imagePath});
    await browser.close();

    console.log("- Dashboard " + dashboardId + " collected.")
    return dashboard;
};

module.exports = saveDashboard;
