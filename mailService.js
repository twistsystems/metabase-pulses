const nodemailer = require("nodemailer");
const config = require('config');

function getHtml(dashboards, questions) {
    let html = '<a href="https://www.twist.systems/"><img src="cid:0" width="204" height="61"/></a><br/><br/>';

    // Adding the dashboards
    for (dashboard of dashboards) {
        html += `<h2>Painel: <a href="${dashboard.externalUrl}">${dashboard.name}</h2></a>
                 <a href="${dashboard.externalUrl}"><img src="cid:${dashboard.id}"/></a>
                 <br/><br/>`
    };

    // Adding the questions
    for (question of questions) {
        html += `<h2>Pergunta: <a href="${question.externalUrl}">${question.name}</h2></a>
                 <a href="${question.externalUrl}"><img src="cid:${question.id}"/></a>
                 <br/><br/>`
    };

    return html;
};

function getAttachments(dashboards, questions) {
    attachments = [{
        filename: 'logo.png',
        path: './logo.png',
        cid: '0'
    }];

    for (dashboard of dashboards) {
        attachments.push({
            filename: `${dashboard.imagePath}`,
            path: `./${dashboard.imagePath}`,
            cid: `${dashboard.id}`
        });
    };

    for (question of questions) {
        attachments.push({
            filename: `${question.imagePath}`,
            path: `./${question.imagePath}`,
            cid: `${question.id}`
        });
    };

    return attachments;
};

async function mailQuestion(subject, text, mailTo, dashboards, questions) {
    let transporter = nodemailer.createTransport({
        host: config.get('smtp.host'),
        port: 587,
        secure: false,
        auth: {
            user: config.get('smtp.user'),
            pass: config.get('smtp.password')
        }
        });

        let info = await transporter.sendMail({
            from: 'TWIST Database <contato@twistsystems.com>',
            to: mailTo,
            subject: subject,
            text: text,
            html: getHtml(dashboards, questions),
            attachments: getAttachments(dashboards, questions)
        });

        console.log("Message sent: %s", info.messageId);
};

module.exports = mailQuestion;
